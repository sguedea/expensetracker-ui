import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Expense } from 'src/app/models/expense';
import { ExpenseService } from 'src/app/services/expense.service';

@Component({
  selector: 'app-add-expense',
  templateUrl: './add-expense.component.html',
  styleUrls: ['./add-expense.component.css']
})
export class AddExpenseComponent implements OnInit {
  // properties
  expense: Expense = {
    id: 0,
    expense: '',
    description: '',
    amount: 0.0
  };

  // dependency injection
  constructor(private expenseService: ExpenseService,
              private router: Router,
              private activeRoute: ActivatedRoute
              ) { }

  ngOnInit(): void {
    var isIdPresent = this.activeRoute.snapshot.paramMap.has('id');
    if (isIdPresent) {
        var expenseId = this.activeRoute.snapshot.paramMap.get('id');
        this.expenseService.getExpense(Number(expenseId)).subscribe(
          data => this.expense = data 
        )
    }
    
  }

  saveExpense() {
    this.expenseService.saveExpense(this.expense).subscribe(
      data => {
        this.router.navigateByUrl("/expenses");
      }
    )
  }

  deleteExpense(id: number) {
    this.expenseService.deleteExpense(id).subscribe(
      data => {
        console.log('deleted response', data);
        this.router.navigateByUrl('/expenses');
      }
    )
  }

  // updateExpense() {
  //   var isIdPresent = this.activeRoute.snapshot.paramMap.has('id');
  //   if (isIdPresent) {
  //       var expenseId = this.activeRoute.snapshot.paramMap.get('id');
  //       this.expenseService.getExpense(Number(expenseId)).subscribe(
  //         data => this.expense = data 
  //       )
  //   }
  // }

  // onUpdatedPost(post: PostHttpClient) {
  //   this.posts.forEach((current, index) => {
  //     if (post.id === current.id) {
  //       this.posts.splice(index, 1);
  //       this.posts.unshift(post);
  //       this.isEdit = false;
  //       this.currentPost = {
  //         id: 0,
  //         title: '',
  //         body: ''
  //       }
  //     }
  //   })
  // }

  

}
