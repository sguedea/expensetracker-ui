import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddExpenseComponent } from './components/add-expense/add-expense.component';
import { ListExpensesComponent } from './components/list-expenses/list-expenses.component';

const routes: Routes = [
  {path: 'expenses', component: ListExpensesComponent},
  {path: 'add-expenses', component: AddExpenseComponent},
  {path: 'edit-expense/:id', component: AddExpenseComponent},
  {path: '', redirectTo: '/expenses', pathMatch: 'full'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
